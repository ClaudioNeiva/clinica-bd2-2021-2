package br.ucsal.bes20212.bd2.clinica;

import java.time.LocalDate;
import java.util.List;

public class Medico {

	private Integer matricula;

	private LocalDate dataAdmissao;

	private String email;

	private List<String> telefones;

	private String numeroCRM;
	
	private UF ufCRM;
	
	private List<Especialidade> especialidades;

	public Medico() {
	}

	public Medico(Integer matricula, String numeroCRM, UF ufCRM, LocalDate dataAdmissao, String email, List<String> telefones,
			List<Especialidade> especialidades) {
		super();
		this.matricula = matricula;
		this.numeroCRM = numeroCRM;
		this.ufCRM = ufCRM;
		this.dataAdmissao = dataAdmissao;
		this.email = email;
		this.telefones = telefones;
		this.especialidades = especialidades;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNumeroCRM() {
		return numeroCRM;
	}

	public void setNumeroCRM(String numeroCRM) {
		this.numeroCRM = numeroCRM;
	}

	public UF getUfCRM() {
		return ufCRM;
	}

	public void setUfCRM(UF ufCRM) {
		this.ufCRM = ufCRM;
	}

	public LocalDate getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(LocalDate dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public List<Especialidade> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<Especialidade> especialidades) {
		this.especialidades = especialidades;
	}

	@Override
	public String toString() {
		return "Medico [matricula=" + matricula + ", numeroCRM=" + numeroCRM + ", ufCRM=" + ufCRM + ", dataAdmissao=" + dataAdmissao
				+ ", email=" + email + ", telefones=" + telefones + ", especialidades=" + especialidades + "]";
	}

	
	
}
