package br.ucsal.bes20212.bd2.clinica;

import java.util.List;

public class Especialidade {

	private Integer codigo;

	private String nome;

	private List<Medico> medicos;

	public Especialidade() {
	}

	public Especialidade(Integer codigo, String nome, List<Medico> medicos) {
		this.codigo = codigo;
		this.nome = nome;
		this.medicos = medicos;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(List<Medico> medicos) {
		this.medicos = medicos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((medicos == null) ? 0 : medicos.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Especialidade other = (Especialidade) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (medicos == null) {
			if (other.medicos != null)
				return false;
		} else if (!medicos.equals(other.medicos))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Especialidade [codigo=" + codigo + ", nome=" + nome + "]";
	}

}
